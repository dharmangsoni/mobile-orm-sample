package com.oogbox.mobileormdemo.db;

import android.content.Context;

import com.oogbox.mobileormdemo.models.Customer;
import com.oogbox.mobileormdemo.models.SysCountry;
import com.oogbox.support.orm.core.helper.MobileORMConfig;

public class DatabaseConfig extends MobileORMConfig {

    public static final String DATABASE_NAME = "MyDatabase.db";

    public DatabaseConfig(Context context) {
        super(context);
        register(Customer.class);
        register(SysCountry.class);
    }

    @Override
    public String getDatabaseName() {
        return DATABASE_NAME;
    }

    @Override
    public String authority() {
        return null;
    }
}
