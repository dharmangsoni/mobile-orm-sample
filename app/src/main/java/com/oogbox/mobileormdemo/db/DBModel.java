package com.oogbox.mobileormdemo.db;

import android.content.Context;

import com.oogbox.support.orm.BaseModel;

/**
 * All your custom method for each model you want.
 * Default it create _id and _write_date columns
 */
public abstract class DBModel extends BaseModel {

    public DBModel(Context context) {
        super(context);
    }
}
