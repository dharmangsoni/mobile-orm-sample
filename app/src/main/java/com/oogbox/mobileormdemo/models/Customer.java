package com.oogbox.mobileormdemo.models;

import android.content.Context;

import com.oogbox.mobileormdemo.db.DBModel;
import com.oogbox.support.orm.core.annotation.DataModel;
import com.oogbox.support.orm.core.types.OBoolean;
import com.oogbox.support.orm.core.types.OManyToOne;
import com.oogbox.support.orm.core.types.OVarchar;

@DataModel("system.customer")
public class Customer extends DBModel {

    OVarchar name = new OVarchar("Name");
    OBoolean active = new OBoolean("Active");
    OManyToOne country_id = new OManyToOne("Country", SysCountry.class);

    public Customer(Context context) {
        super(context);
    }
}
