package com.oogbox.mobileormdemo.models;

import android.content.Context;

import com.oogbox.mobileormdemo.db.DBModel;
import com.oogbox.support.orm.core.annotation.DataModel;
import com.oogbox.support.orm.core.types.OVarchar;

@DataModel("sys.country")
public class SysCountry extends DBModel {

    OVarchar name = new OVarchar("Name");

    public SysCountry(Context context) {
        super(context);
    }
}
