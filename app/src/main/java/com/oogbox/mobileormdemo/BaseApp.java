package com.oogbox.mobileormdemo;

import android.app.Application;

import com.oogbox.mobileormdemo.db.DatabaseConfig;
import com.oogbox.support.orm.MobileORM;

public class BaseApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        MobileORM.init(new DatabaseConfig(this));
    }
}
