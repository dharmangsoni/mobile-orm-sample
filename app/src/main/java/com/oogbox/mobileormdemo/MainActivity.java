package com.oogbox.mobileormdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.oogbox.mobileormdemo.models.Customer;
import com.oogbox.support.orm.core.data.ORecordValue;
import com.oogbox.support.orm.core.data.RelationValue;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Customer customer = new Customer(this);
        if (customer.count(null, null) > 0) {
            Log.e(">>", "Customers" + customer.select());
        } else {
            ORecordValue value = new ORecordValue();
            value.put("name", "John Doe");

            ORecordValue india = new ORecordValue();
            india.put("name", "India");
            // You can directly add new object or id if you have
            value.put("country_id", india);
            value.put("active", true);
            int newId = customer.create(value);
            Log.v(">>", "New customer created #" + newId);
            Log.e(">>>", customer.browse(newId) + "<<");
        }
    }
}
