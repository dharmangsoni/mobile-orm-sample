# Mobile ORM Sample 

Add library to your app.gradle
-------------------------------

``implementation 'com.oogbox.support:mobile-orm:1.0.1'``


Create mobile orm config file 
------------------------------

```java
import android.content.Context;

import com.oogbox.mobileormdemo.models.Customer;
import com.oogbox.mobileormdemo.models.SysCountry;
import com.oogbox.support.orm.core.helper.MobileORMConfig;

public class DatabaseConfig extends MobileORMConfig {

    public static final String DATABASE_NAME = "MyDatabase.db";

    public DatabaseConfig(Context context) {
        super(context);
		// You need to create this class before register here
        register(Customer.class);
        register(SysCountry.class);
    }

    @Override
    public String getDatabaseName() {
        return DATABASE_NAME;
    }

    @Override
    public String authority() {
        return null;
    }
}
```

Create Application Class and add to manifest
---------------------------------------------

```java
import android.app.Application;

import com.oogbox.mobileormdemo.db.DatabaseConfig;
import com.oogbox.support.orm.MobileORM;

public class BaseApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
		// Register your database config here
        MobileORM.init(new DatabaseConfig(this));
    }
}
```

```xml

    <application
        android:name=".BaseApp"
        android:allowBackup="true"
        android:icon="@mipmap/ic_launcher"
        android:label="@string/app_name"
        android:roundIcon="@mipmap/ic_launcher_round"
        android:supportsRtl="true"
        android:theme="@style/AppTheme">
```

Register your database version in manifest
------------------------------------------

```xml
   <application
        android:name=".BaseApp"
        android:allowBackup="true"
        android:icon="@mipmap/ic_launcher"
        android:label="@string/app_name"
        android:roundIcon="@mipmap/ic_launcher_round"
        android:supportsRtl="true"
        android:theme="@style/AppTheme">

        <meta-data
            android:name="DATABASE_VERSION"
            android:value="1" />

```

Create one model class which extends BaseModel 
-----------------------------------------------

```java
import android.content.Context;

import com.oogbox.support.orm.BaseModel;

/**
 * All your custom method for each model you want.
 * Default it create _id and _write_date columns
 */
public abstract class DBModel extends BaseModel {

    public DBModel(Context context) {
        super(context);
    }
}
```

This model is extended in each of your model. and also contain your commonly required methods and logics. 
If you need some columsn to add default in each model specify those column in this model class.

Create your model (data table) extending your model class
----------------------------------------------------------

```java
import com.oogbox.mobileormdemo.db.DBModel;
import com.oogbox.support.orm.core.annotation.DataModel;
import com.oogbox.support.orm.core.types.OBoolean;
import com.oogbox.support.orm.core.types.OManyToOne;
import com.oogbox.support.orm.core.types.OVarchar;

@DataModel("system.customer")
public class Customer extends DBModel {

    OVarchar name = new OVarchar("Name");
    OBoolean active = new OBoolean("Active");
    OManyToOne country_id = new OManyToOne("Country", SysCountry.class);

    public Customer(Context context) {
        super(context);
    }
}

```
Note: Do not forgot to register your model in DatabaseConfig file when you create. 

You can see sample project with this all configuration.